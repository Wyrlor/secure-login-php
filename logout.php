<?php

require 'functions.php';

if (isset($_SESSION['user_id'])) {
    $user = new User($pdo);
    $user->logout();
} else {
    header('location: check.php');
}
?>
