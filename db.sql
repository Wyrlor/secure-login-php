DROP TABLE securelogin
CREATE TABLE securelogin (
   `id`           INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
   `username`     VARCHAR(255) NOT NULL,
   `password`     VARCHAR(255) NOT NULL,
   `date_created` DATETIME,
   `last_logged`  DATETIME,
   `first_failed` DATETIME,
   `failed_count` INT(8),
   `locked`       INT(8),
   UNIQUE (`username`)
) ENGINE = MYISAM;
