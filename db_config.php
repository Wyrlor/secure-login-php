<?php
session_start();

date_default_timezone_set('America/New_York');

define('DRIVER', 'mysql');
define('HOST', 'localhost');
define('USER', 'root');
define('PASSWORD', '');
define('DATABASE', 'tutorial');

try {
    // mysql:host=localhost;dbname=secureLogin, root,
    $pdo = new PDO(DRIVER . ':host=' . HOST . ';dbname=' . DATABASE . '', USER, PASSWORD);
} catch (Exception $e) {
    echo 'Connection failed: ' . $e->getMessage();
    die();
}
