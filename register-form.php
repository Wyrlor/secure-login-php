<?php
// Include the controller logic file
require 'functions.php';

if (!isset($_SESSION['_csrf_token'])) {
    $_SESSION['_csrf_token'] =  csrf_generator();
}

$csrf_token = $_SESSION['_csrf_token'];

// Print any errors that occurred
if (isset($errors)) {
    echo 'There were the following errors:
            <ul>';
    foreach ($errors as $error) {
        echo '<li>' . $error . '</li>';
    }
    echo '</ul>';
}
?>

<form name="register_form" method="post">
    <input type="text" name="username" placeholder="username"/>
    <input type="password" name="password" placeholder="password"/>
    <input type="password" name="confirm_pass" placeholder="confirm password"/>
    <input type="submit" name="register_submit" value="submit"/>

    <input type="hidden" name="csrf_token" value="<?= $csrf_token; ?>" />
</form>
