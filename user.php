<?php

class User
{
    // Holds the information for the database connection
    protected $db;
    protected $hash_options = ['cost' => 12];
    protected $lock_options = ['max_failures' => 10, 'failure_time' => 600];

    /**
     * Construct the class with the database information
     * @param [object] $pdo [holds the database connection information]
     */
    public function __construct($pdo)
    {
        $this->db = $pdo;
    }

    /**
     * Registers the user in the database
     * @param  [string] $username [username entered in the form by the user]
     * @param  [string] $password [password entered in the form by the user]
     * @return [bool]             [returns false if there are any storing errors]
     */
    public function register($username, $password)
    {
        try {
            $user_exists = User::getByUsername($username);
            if (!$user_exists) {
                $password = password_hash($password, PASSWORD_DEFAULT, $this->hash_options);

                $query = 'INSERT INTO securelogin (username, password, date_created) VALUES (:username, :password, NOW())';
                $stmt = $this->db->prepare($query);
                $stmt->bindParam(':username', $username, PDO::PARAM_STR);
                $stmt->bindParam(':password', $password, PDO::PARAM_STR);
                $stmt->execute();

                return true;
            }
            return false;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getByUsername($username)
    {
        $query = "SELECT * FROM securelogin WHERE username = :username";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ($result) {
            return (object)$result;
        }
        return false;
    }

    /**
     * Checks the entered information and logs in if the user exists
     * @param  [string] $username [username entered in the form by the user]
     * @param  [string] $password [password entered in the form by the user]
     * @return [bool]             [returns false if there are any retriving errors]
     */
    public function login($username, $password)
    {
        $user = User::getByUsername($username);

        if ($user) {
            if ($user->locked === 1) {
                header('location: locked.php');
                exit();
            } else {
                if (password_verify($password, $user->password)) {
                    // if PASSWORD_DEFAULT changes, update password encrypting alg
                    if (password_needs_rehash($user->password, PASSWORD_DEFAULT, $this->hash_options)) {
                        User::updatePassword($username, $password);
                    }
                    $id = htmlspecialchars($user->id);
                    $username = htmlspecialchars($user->username);

                    $_SESSION['user_id'] = $id;
                    $_SESSION['username'] = $username;

                    $query = "UPDATE securelogin SET last_logged = NOW()";
                    $stmt = $this->db->prepare($query);
                    $stmt->execute();

                    return true;
                } else {
                    if ($user->failed_count >=  $this->lock_options['max_failures'] && time() - strtotime($user->first_failed) <  $this->lock_options['failure_time']) {
                        User::lockAccount($user->id);
                    } else if (time() - strtotime($user->first_failed) > $this->lock_options['failure_time']) {
                        $query = "UPDATE securelogin SET first_failed = NOW(), failed_count = 1 WHERE id = :id";
                        $stmt = $this->db->prepare($query);
                        $stmt->bindParam(':id', $user->id, PDO::PARAM_INT);
                        $stmt->execute();

                    } else {
                        $query = "UPDATE securelogin SET failed_count = failed_count + 1 WHERE id = :id";
                        $stmt = $this->db->prepare($query);
                        $stmt->bindParam(':id', $user->id, PDO::PARAM_INT);
                        $stmt->execute();
                    }
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * Locks the users account if there are too many login attempts
     * @param  [int] $id [the id of the user whose account is being locked]
     */
    public function lockAccount($id)
    {
        $query = "UPDATE securelogin SET locked = 1 WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    /**
     * Updates the password if the encrypting method changes
     * @param  [string] $username [username entered in the form by the user]
     * @param  [string] $password [password entered in the form by the user]
     */
    public function updatePassword($username, $password)
    {
        $password = password_hash($password, PASSWORD_DEFAULT, $this->hash_options);

        $query = "UPDATE securelogin SET password = :password WHERE username = :username";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(':username', $username, PDO::PARAM_STR);
        $stmt->bindParam(':password', $password, PDO::PARAM_STR);
        $stmt->execute();
    }

    /**
     * Logs the user out by destroying all sessions and redirects to homepage
     */
    public function logout()
    {
        session_destroy();

        header('location: index.php');
        exit();
    }
}
