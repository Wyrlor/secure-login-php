<?php
// Include the database information and User class
require 'db_config.php';
require 'user.php';

/**
 * @return string
 *
 * Generate a csrf token for form submission
 */
function csrf_generator()
{
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));
    } else {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }
}

// If the registration form has been submitted
if (isset($_POST['register_submit'])) {

    // Create a new instance of the User class and an ampty errors array
    $user = new User($pdo);
    $errors = [];

    // Strip HTML code from the inputs and force username to be lowercase
    $username = htmlspecialchars($_POST['username']);
    $username = strtolower($username);
    $password = htmlspecialchars($_POST['password']);
    $confirm_pass = htmlspecialchars($_POST['confirm_pass']);
    $csrf_check = $_POST['csrf_token'];

    // Check for errors in the data submitted from the form
    if (!ctype_alnum($username)) {
        $errors['inv_name'] = 'Username can only be letters or numbers';
    }
    if (strlen($username) < 4) {
        $errors['short_name'] = 'Username too short';
    }
    if (strlen($password) < 6) {
        $errors['short_pass'] = 'Password too short';
    }
    if ($password !== $confirm_pass) {
        $errors['bad_pass'] = 'Passwords must match';
    }
    if ($_SESSION['_csrf_token'] != $csrf_check) {
        $errors['csrf'] = 'An unexpected error occured';
    }
    // Register the user if there are no errors caught
    if (empty($errors)) {
        if ($user->register($username, $password)) {
            header('location: index.php');
            exit();
        }

        // Return to the form if there was an unforseen problem registering
        header('location: register-form.php');
        exit();
    }
}

// If the registration form has been submitted
if (isset($_POST['login_submit'])) {
    // Create a new instance of the User class and an ampty errors array
    $user = new User($pdo);
    $errors = array();

    // Strip HTML code from the inputs and force username to be lowercase
    $username = htmlspecialchars($_POST['username']);
    $username = strtolower($username);
    $password = htmlspecialchars($_POST['password']);

    // Attempt to log user in, if there are problems registering throw an error
    if ($user->login($username, $password)) {
        header('location: check.php');
        exit();
    } else {
        $errors['inv_login'] = 'Your username and password did not match';
    }
}
