<?php

// Include the controller logic file
require 'functions.php';

// Print any errors that occurred
if (isset($errors)) {
    echo 'There were the following errors:
            <ul>';
    foreach ($errors as $error) {
        echo '<li>' . $error . '</li>';
    }
    echo '</ul>';
}
?>

<form name="login_form" method="post">
    <input type="text" name="username" placeholder="username"/>
    <input type="password" name="password" placeholder="password"/>
    <input type="submit" name="login_submit" value="submit"/>
</form>
